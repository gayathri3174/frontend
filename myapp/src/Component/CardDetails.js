import React from 'react'
import { useParams } from 'react-router-dom'

function CardDetails() {
    const {id } = useParams();
  return (
    <div>
     <h1>hello {id}</h1>
    </div>
  )
}

export default CardDetails