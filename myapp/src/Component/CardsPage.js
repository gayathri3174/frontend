import React, { useState } from 'react'
import { Card, CardTitle, Button, CardBody, CardText, CardSubtitle } from 'reactstrap';


function CardsPage() {

    const [data, setData] = useState([{
        cardtitile: "iphone",
        cardSubtitile: "iphone 15 Pro ",
        cardText: "8 GB , 256 Gb storage",
        button: "View Details",
        imgurl:"https://cdn.pixabay.com/photo/2016/11/29/05/08/apple-1867461_1280.jpg",
        


    }, {
        cardtitile: "ipad",
        cardSubtitile: "iphone 15 Pro ",
        cardText: "8 GB , 256 Gb storage",
        button: "View Details",
        imgurl:"https://fdn2.gsmarena.com/vv/pics/apple/apple-ipad-air-1.jpg"


    }, {
        cardtitile: "iwatch",
        cardSubtitile: "iphone 15 Pro ",
        cardText: "8 GB , 256 Gb storage",
        button: "View Details",
        imgurl:""


    },
    ])
    return (
        <div style={{display:'flex' ,justifyContent:'space-around'}} >
            {
                data.map((item) => {
                    return (
                        <div>
                            <Card
                style={{
                    width: '18rem'
                }}
            >
                <img
                    alt="Sample"
                    src={item.imgurl}
                />
                <CardBody>
                    <CardTitle tag="h5">
                        {item.cardtitile}
                    </CardTitle>
                    <CardSubtitle
                        className="mb-2 text-muted"
                        tag="h6"
                    >
                        {item.cardSubtitile}
                    </CardSubtitle>
                    <CardText>
                        {item.cardText}
                    </CardText>
                    <Button>
                        {item.button}
                    </Button>
                </CardBody>
            </Card>
                        </div>
                    )
                })
            }


        </div>
    )
}

export default CardsPage