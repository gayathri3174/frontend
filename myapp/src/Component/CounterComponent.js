import React from 'react'
import { useSelector,useDispatch } from 'react-redux'
import counterReducer from '../reducer/counterreducer'
import { increment } from '../actions';
import { Button,CardTitle,CardSubtitle,CardBody,Card,CardText } from 'reactstrap';


function CounterComponent() {
    let dispatch = useDispatch()
    let carddata= useSelector((state) => state.counterReducer);
  
    return (
        <div>
           
           {
           carddata.map((item)=>(
            <div key={item.id}>
            <Card
         style={{
             width: '18rem'
         }}
     >
         <img
             alt="Sample"
             src={item.imgurl}
         />
         <CardBody>
             <CardTitle tag="h5">
                 {item.title}
             </CardTitle>
             <CardSubtitle
                 className="mb-2 text-muted"
                 tag="h6"
             >
                 {item.subtitle}
             </CardSubtitle>
             <CardText>
                 {item.cardsdiscription}
             </CardText>
             <Button>
                 {item.buttontext}
             </Button>
         </CardBody>
     </Card>
         </div>

           ))
         }
        </div>
    );
}

export default CounterComponent