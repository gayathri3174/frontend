import React, { useState } from 'react';
import birdlogo from '../assests/images/birdlogo.png'
import Vanamo_Logo from '../assests/images/Vanamo_Logo.png';
import Login from './Login'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';

function NavbarComponent(props) {
  const [collapsed, setCollapsed] = useState(true);

  const toggleNavbar = () => setCollapsed(!collapsed);

  return (
    <div>
      <Navbar color="faded" light>
        <NavbarBrand href="/" className="me-auto">
        <img
                            alt="logo"

                            src={Vanamo_Logo}
                            style={{
                                height: 40,
                                width: 40,

                            }}
                        />
        </NavbarBrand>
       <Login />
        <NavbarToggler onClick={toggleNavbar} className="me-2" />
        <Collapse isOpen={!collapsed} navbar>
          <Nav navbar>
            <NavItem>
              <NavLink href="/components/">Components</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="https://github.com/reactstrap/reactstrap">
                GitHub
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default  NavbarComponent;